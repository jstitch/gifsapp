import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Gif, SearchGifsResponse } from '../interface/gifs.interface';

@Injectable({
    // no hace falta meterlo al providers del module, esta disponible en toda la aplicacion
    providedIn: 'root'
})
export class GifsService {
    private _apiKey: string = 'DRSjjPMiq4hrDogGQQUMv1WrkiPzNKYu';
    private _servicioUrl: string = 'https://api.giphy.com/v1/gifs';
    private _historial: string[] = [];

    public resultados: Gif[] = [];

    get historial() {
        return [...this._historial];
    }

    constructor(private http: HttpClient) {
        this._historial = JSON.parse(localStorage.getItem('historial')!) || []
        this.resultados = JSON.parse(localStorage.getItem('resultados')!) || []
    }

    // async buscarGifs(query: string = '') {
    buscarGifs(query: string = '') {
        query = query.trim().toLocaleLowerCase();
        if (!this._historial.includes(query)) {
            this._historial.unshift(query);
            this._historial = this._historial.splice(0, 10);

            localStorage.setItem("historial", JSON.stringify(this._historial));
        }

        // con async-await
        // const resp = await fetch('https://api.giphy.com/v1/gifs/search?api_key=DRSjjPMiq4hrDogGQQUMv1WrkiPzNKYu&q=Star wars&limit=10');
        // const data = await resp.json();
        // console.log(data);

        // directo fecth con promesa
        // fetch('https://api.giphy.com/v1/gifs/search?api_key=DRSjjPMiq4hrDogGQQUMv1WrkiPzNKYu&q=Star wars&limit=10')
        //     .then(resp => {
        //         resp.json().then(data => {
        //             console.log(data)
        //         })
        //     })

        // con HttpClientModule (observables)
        const params = new HttpParams()
            .set('api_key', this._apiKey)
            .set('limit', '10')
            .set('q', query);
        console.log(params.toString());
        this.http.get<SearchGifsResponse>(`${this._servicioUrl}/search`, { params }) // identico a {params: params} por ser el mismo nombre la key que el valor
            .subscribe((resp) => {
                // console.log(resp.data);
                this.resultados = resp.data;
                localStorage.setItem("resultados", JSON.stringify(this.resultados));
            })
    }
}
