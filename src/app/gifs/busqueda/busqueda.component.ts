import { Component, ElementRef, ViewChild } from '@angular/core';
import { GifsService } from '../services/gifs.service';

@Component({
    selector: 'app-busqueda',
    templateUrl: './busqueda.component.html',
    styles: [
    ]
})
export class BusquedaComponent {

    // ! : non-null assertion operator https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-0.html#non-null-assertion-operator
    @ViewChild('txtBuscar') txtBuscar!: ElementRef<HTMLInputElement>;

    constructor(private gifsService: GifsService) {
    }

    buscar() {
        // console.log(termino);
        // document.querySelector('input')?.value = "";

        const valor = this.txtBuscar.nativeElement.value;

        if (valor.trim().length == 0) {
            return;
        }

        this.gifsService.buscarGifs(valor);

        // this.txtBuscar.nativeElement.select();

        this.txtBuscar.nativeElement.value = '';
    }
}
